%global modname cerberus

Name:           python-cerberus
Version:        1.1
Release:        1%{?dist}
Summary:        Lightweight, extensible data validation library for Python

License:        ISC
URL:            https://github.com/nicolaiarocci/cerberus
Source0: %{name}-%{version}.tar.gz
BuildArch:      noarch

%global _description \
Cerberus is a lightweight and extensible data validation library for Python.\
\
Cerberus provides type checking and other base functionality out of the box\
and is designed to be non-blocking and easily extensible, allowing for custom\
validation. It has no dependancies and is thoroughly tested.

%description %{_description}

%package -n python2-%{modname}
Summary:        %{summary}
%{?python_provide:%python_provide python2-{%modname}}
BuildRequires:  python-devel
BuildRequires:  python-setuptools
BuildRequires:  pytest

%description -n python2-%{modname} %{_description}

Python 2 version.

%package -n python3-%{modname}
Summary:        %{summary}
%{?python_provide:%python_provide python%{python3_pkgversion}-%{modname}}
BuildRequires:  python34-devel
BuildRequires:  python34-setuptools
BuildRequires:  python34-pytest

%description -n python3-%{modname} %{_description}

Python 3 version.

%prep
%autosetup -n %{modname}-%{version}

%build
%{__python2} setup.py build
%if 0%{?with_python3}
%{__python3} setup.py build
%endif

%install
%if 0%{?with_python3}
%{__python3} setup.py install --single-version-externally-managed -O1 --root=$RPM_BUILD_ROOT --record=INSTALLED_FILES_3
%endif

%{__python2} setup.py install --single-version-externally-managed -O1 --root=$RPM_BUILD_ROOT --record=INSTALLED_FILES_2
rm -f html/.buildinfo

%check
py.test-%{python2_version} -v %{modname}/tests
py.test-%{python3_version} -v %{modname}/tests

%files -n python2-%{modname}
%license LICENSE
%doc README.rst AUTHORS CHANGES
%{python2_sitelib}/*

%if 0%{?with_python3}
%files -n python3-%{modname}
%license LICENSE
%doc README.rst AUTHORS CHANGES
%{python3_sitelib}/*
%endif # with_python3

%changelog
* Sat Feb 11 2017 Igor Gnatenko <ignatenkobrain@fedoraproject.org> - 1.1-1
- Update to 1.1

* Sat Feb 11 2017 Fedora Release Engineering <releng@fedoraproject.org> - 1.0.1-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_26_Mass_Rebuild

* Mon Dec 19 2016 Miro Hrončok <mhroncok@redhat.com> - 1.0.1-2
- Rebuild for Python 3.6

* Thu Sep 01 2016 Igor Gnatenko <ignatenko@redhat.com> - 1.0.1-1
- Update to 1.0.1

* Tue Jul 19 2016 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.9.2-2
- https://fedoraproject.org/wiki/Changes/Automatic_Provides_for_Python_RPM_Packages

* Mon May 09 2016 Igor Gnatenko <ignatenko@redhat.com> - 0.9.2-1
- Initial package
